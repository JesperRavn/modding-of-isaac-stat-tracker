import requests
from BeautifulSoup import BeautifulSoup
import os
import io
import json
import datetime
import plotly
import plotly.graph_objs as go


class isaac(object):
    def __init__(self, user):
        self.user = user

    def __datainstance(self, number, timestamp):
        return {'Number': number, 'Timestamp': timestamp}

    def __checkfile(self):
        if 'data.json' not in os.listdir(os.getcwd()):
            with io.FileIO("data.json", "w") as file:
                data = {'mods': []}
                json.dump(data, file, sort_keys=True,
                          ensure_ascii=True, indent=4)

    def __createmod(self, name, data):
        mod = {
            'Name': name,
            'Downloads': [],
            'Hearts': [],
            'Comments': []
        }
        return data['mods'].append(mod)

    def __loaddata(self):
        with open('data.json', 'r') as data_file:
            data = json.load(data_file)
            return data

    def __savedata(self, data):
        with open('data.json', 'w') as outfile:
            json.dump(data, outfile, sort_keys=True,
                      ensure_ascii=True, indent=4)

    def __modexists(self, name, data):
        for mod in data['mods']:
            if name in mod['Name']:
                return True
        return False

    def __appenddata(self, instance, category, mod, data):
        for currentmod in data['mods']:
            if currentmod['Name'] == mod:
                currentmod[category].append(instance)
                self.__savedata(data)
                return data
                break

    def plot(self, destination, *args):
        data = self.__loaddata()
        graphs = []
        for arguement in args:
            for mod in data['mods']:
                x = []
                y = []
                for heart in mod[arguement]:
                    x.append(heart['Timestamp'])
                    y.append(heart['Number'])
                name1 = mod['Name'] + " - " + arguement
                trace = go.Scatter(x=x, y=y, mode='lines', name=name1)
                graphs.append(trace)
        plotly.offline.plot(graphs, filename=destination, auto_open=False)

    def update(self):
        self.__checkfile()
        data = self.__loaddata()
        response = requests.get('http://moddingofisaac.com/user/' + self.user)
        timestamp = str(datetime.datetime.now())
        html = response.content
        soup = BeautifulSoup(html)
        test = soup.findAll('div', {'class': 'item-container glow'})

        for i in test:
            for tag in i.findAll('span'):
                tag.replaceWith('')
            name = i.find('div', {'class': 'name truncate'}).text
            downloads = i.find('div', {'data-tooltip': 'Downloads'}).text
            hearts = i.find('div', {'data-tooltip': 'Hearts'}).text
            comments = i.find('div', {'data-tooltip': 'Comments'}).text

            if self.__modexists(name, data):
                data = self.__appenddata(self.__datainstance(
                    downloads, timestamp), 'Downloads', name, data)
                data = self.__appenddata(self.__datainstance(
                    hearts, timestamp), 'Hearts', name, data)
                data = self.__appenddata(self.__datainstance(
                    comments, timestamp), 'Comments', name, data)
            else:
                self.__createmod(name, data)
                data = self.__appenddata(self.__datainstance(
                    downloads, timestamp), 'Downloads', name, data)
                data = self.__appenddata(self.__datainstance(
                    hearts, timestamp), 'Hearts', name, data)
                data = self.__appenddata(self.__datainstance(
                    comments, timestamp), 'Comments', name, data)
