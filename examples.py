from isaac import isaac
''' 
The class isaac needs the ID of the profile page 
ex: https://moddingofisaac.com/user/@leblarh
'''
user = isaac('@leblarh') 

'''
Scrapes the profile of the given user
and saves it to data.json file.
 '''
user.update() 
''' 
plot(path, *args)
Exports a graph to the given path in HTML format, 
with the plots given as arguements.  
'''
user.plot('mods.html', 'Downloads', 'Hearts', 'Comments') 