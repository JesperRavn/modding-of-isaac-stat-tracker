# Summary #
This is a simple tool for python, to track the progression of ones mods on the website [ModdingofIsaac.com](moddingofisaac.com)
# Dependencies #
* BeautifulSoup
* Plotly

# Installation #
1. Download isaac.py 
2. Install dependencies
3. Import the class in your program 
### Plotly ###
```
#!Bash

sudo pip install plotly
```

### Beautiful soup ###
```
#!Bash

sudo pip install beautifulsoup4
```



# Examples #

```
#!python

from isaac import isaac
''' 
The class isaac needs the ID of the profile page 
ex: https://moddingofisaac.com/user/@leblarh
'''
user = isaac('@leblarh') 

'''
Scrapes the profile of the given user
and saves it to data.json file.
 '''
user.update() 
''' 
plot(path, *args)
Exports a graph to the given path in HTML format, 
with the plots given as arguements.  
'''
user.plot('mods.html', 'Downloads', 'Hearts', 'Comments') 
```

# Authors #
* Jesper Ravn-Nielsen



*last edit: 1/7/2016*